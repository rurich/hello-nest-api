## Build Stage
FROM node:lts AS builder
WORKDIR /app
COPY ./package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build


## Deploy Stage
FROM node:lts-alpine
WORKDIR /app
COPY --from=builder /app ./
EXPOSE 3001
CMD ["yarn", "start:prod"]