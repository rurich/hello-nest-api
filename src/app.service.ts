import { Injectable } from '@nestjs/common';

export type HelloResponse = {
  message: string;
};

@Injectable()
export class AppService {
  getHello(): HelloResponse {
    return {
      message: 'Welcome to Next and Nest!',
    };
  }
}
